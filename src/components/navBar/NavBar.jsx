import React, { Component } from 'react';
import { Navbar, Nav, NavItem } from 'react-bootstrap';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import NewDialogue from '../dialogueView/newDialogue/NewDialogue';
import CurrentDialogue from '../dialogueView/currentDialogue/CurrentDialogue';
import Archive from '../dialogueView/archive/Archive';
import DialogueView from '../dialogueView/DialogueView';

import './NavBar.css';

class NavBar extends Component {
  render() {
    return (<Navbar default="default" collapseOnSelect="collapseOnSelect">
      <Navbar.Header>
        <Navbar.Brand>
          <Link to="/">Kvicka</Link>
        </Navbar.Brand>
        <Navbar.Toggle/>
      </Navbar.Header>
      <Navbar.Collapse>
        <Nav pullRight="pullRight">
          <NavItem eventKey={1} componentClass={Link} href="/Dialogues" to="/Dialogues">
            Dialogues
          </NavItem>
          <NavItem eventKey={2} componentClass={Link} href="/NewDialogue" to="/NewDialogue">
            newDialogue
          </NavItem>
          <NavItem eventKey={3} componentClass={Link} href="/Archive" to="/Archive">
            Archive
          </NavItem>
        </Nav>
      </Navbar.Collapse>
    </Navbar>);
  }
};

export default NavBar;

/**
 *         <li><link>home</link></li>
         <li><link>new</link></li>
 */
