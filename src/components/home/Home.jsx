import React, { Component } from 'react';
import './Home.css';
import { Link } from 'react-router-dom';
import { Jumbotron, Grid, Row, Col, Image, Button } from 'react-bootstrap';

class Home extends Component {
  render() {
    return (
      <Grid>
        <Jumbotron>
          <h2>Home</h2>
        </Jumbotron>
        <Row className="show-grid text-center">
          <Col xs={12} sm={4} className="dialogue-wrapper">
            <Image src="./assets/appspotr-logo-new-white.png" circle className="profile-pic" />
            <h3>hello there</h3>
            <p>welcome to spooktober</p>
          </Col>
        </Row>
      </Grid>
    );
  }
}

export default Home;
