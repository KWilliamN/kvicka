import React, { Component } from 'react';
import './DialogueCard.css';
import { subscribeToTimer } from '../../../api';

class DialogueCard extends Component {

  constructor(props) {
    super(props);
    subscribeToTimer((err, timestamp) => this.setState({ timestamp }));
  }

  state = {
    timestamp: 'no timestamp yet',
    id: undefined,
    created: undefined,
    subject: undefined,
    tag: undefined,
    creator: undefined,
    firstname: undefined,
    lastname: undefined,
    phonenumber: undefined,
    notes: undefined,
    state: undefined,
    regnumber: undefined,

  };

  getData = async (e) => {
    e.preventDefault();
    const id = e.target.elements.id.value;
    const created = e.target.elements.created.value;
    const subject = e.target.elements.subject.value;
    const tag = e.target.elements.tag.value;
    const creator = e.target.elements.creator.value;
    const firstname = e.target.elements.firstname.value;
    const lastname = e.target.elements.lastname.value;
    const phonenumber = e.target.elements.phonenumber.value;
    const notes = e.target.elements.notes.value;
    const state = e.target.elements.state.value;
    const regnumber = e.target.elements.regnumber.value;
    const api_call = await fetch(`http://http://kvicka.hiqcould.net:8085/swagger/index.html#!/dialogueController/get_dialogue_id${id},${created},${subject},${tag},${creator},${firstname},${lastname},${phonenumber},${notes},${state},${regnumber},&units=metric`);
    const data = await api_call.json();
    if (id && created && subject && tag && creator && firstname && lastname && phonenumber && notes && state && regnumber) {
       this.setState({

       });
    } else {

    }
  };

  render() {
    return (
      <div className="DialogueCard">
      <h2>{this.state.timestamp}</h2>
    </div>
  );
  }
}

export default DialogueCard;
