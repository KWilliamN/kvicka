import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import {
  Jumbotron,
  Grid,
  Row,
  Col,
  Image,
  Button
} from 'react-bootstrap';
import './DialogueView.css';
import NewDialogue from './newDialogue/NewDialogue';
import CurrentDialogue from './currentDialogue/CurrentDialogue';
import Archive from './archive/Archive';
import { subscribeToTimer } from '../../api';
import DialogueCard from './dialogueCard/DialogueCard';

class DialogueView extends Component {

  constructor(props) {
    super(props);
    subscribeToTimer((err, timestamp) => this.setState({
      timestamp,
    }));
  }

  state = {
    timestamp: 'no timestamp yet',
  };

  render() {
    return (
      <div className="DialogueView" xs="6" sm="4">
        <div className="newDialogues">
          <DialogueCard/>
          <DialogueCard/>

        </div>
        <div className="allDialogues">

        </div>

      </div>
  );}
}

export default DialogueView;

/**
 *         <NewDialogue />
         <CurrentDialogue />
         <History />
 */
