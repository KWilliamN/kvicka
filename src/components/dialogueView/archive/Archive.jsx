import React, { Component } from 'react';
import './Archive.css';
import { Link } from 'react-router-dom';
import { Jumbotron, Grid, Row, Col, Image, Button } from 'react-bootstrap';

class Archive extends Component {
  render() {
    return (
      <Grid>
        <Jumbotron>
          <h2>Archive</h2>
          <Link to="/">
            <Button BsStyle="primary">Back</Button>
          </Link>
        </Jumbotron>

      </Grid>
    );
  }
}

export default Archive;
