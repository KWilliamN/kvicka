import React, { Component } from 'react';

class Dropdown extends Component {
  constructor(props) {
    super(props)
    this.state = {
      listOpen: false,
      headerTitle: this.props.title
    }
  }

  static getDerivedStateFromProps(nextProps){
    const count = nextProps.list.filter(function(a) { return a.selected; }).length;
    if (count === 0) {
      return { headerTitle: nextProps.title}
    } else if (count === 1) {
      return { headerTitle: `${count} ${nextProps.titleHelper}`}
    } else if (count > 1) {
      return { headerTitle: `${count} ${nextProps.titleHelper}s`}
    }
  }

  toggleList() {
    this.setState(prevState => ({
      listOpen: !prevState.listOpen
    }))
  }

  render() {
    const { list } = this.props
    const { listOpen, headerTitle } = this.state
    return (<div className="dd-wrapper">
      <div className="dd-header" onClick={() => this.toggleList()}>
        <div className="dd-header-title">{headerTitle}</div>

      </div>
      {
        listOpen && <ul className="dd-list">
            {list.map((item) => (<li className="dd-list-item" key={item.id}>{item.title}</li>))}
          </ul>
      }
    </div>
    )
  }

}

export default Dropdown;
