import React, { Component } from 'react';
import './NewDialogue.css';
import Dropdown from './dropdown/Dropdown';
import { Link } from 'react-router-dom';
import {
  Jumbotron,
  Grid,
  Row,
  Col,
  Image,
  Button
} from 'react-bootstrap';

class NewDialogue extends Component {
  constructor () {
    super ()
    this.state = {
      location: [
        {
          id: 0,
          title: 'California',
          selected: false,
          key: 'location'
        },
        {
          id: 1,
          title: 'Dublin',
          selected: false,
          key: 'location'
        },
        {
          id: 2,
          title: 'California',
          selected: false,
          key: 'location'
        }
      ]
    }
  }

  toggleSelected = (id, key) => {
    let temp = [...this.state[key]]
    temp[id].selected = !temp[id].selected
    this.setState({
      [key]: temp
    })
  }

  render() {
    return (<Grid>
      <h2 className="Title">New dialogue</h2>

      <Jumbotron>
        <div class="form-row">
          <div class="form-group col-md-6">
            <label for="inputFirstname">Full name</label>
            <input type="text" class="form-control" placeholder="Full name"/>
          </div>
          <div class="form-group col-md-6">
            <label for="inputLastname">Subject</label>
            <input type="text" class="form-control" placeholder="Subject"/>
          </div>
        </div>
        <div className="form-group">
          <label for="exampleFormControlTextarea1">Phone number</label>
          <input type="email" className="form-control" id="exampleInputEmail1" placeholder="Enter phone number"></input>
        </div>
        <div className="form-group">
          <label for="exampleInputEmail1">Email address</label>
          <input type="email" className="form-control" id="exampleInputEmail1" placeholder="name@email.com"></input>
          <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
        </div>
        <div class="form-group">
          <label for="exampleFormControlTextarea1">Description</label>
          <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
        </div>
        <select class="form-control">
          <option>Övrigt</option>
          <option>Fakturor</option>
          <option>IT</option>
          <option>Internt</option>
          <option>HR</option>
        </select>
        <div class="form-check">
          <button type="submit" class="btn btn-primary">Submit</button>
        </div>
      </Jumbotron>
      <form>



        <div class="form-check">
          <Link to="/">
            <Button BsStyle="primary">Back</Button>
          </Link>
        </div>
      </form>

    </Grid>);
  }
}

/**
 * import Dropdown from './dropdown/Dropdown';

 */

export default NewDialogue;
