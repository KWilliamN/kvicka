import React, { Component } from 'react';
import DialogueView from './components/dialogueView/DialogueView';
import NewDialogue from './components/dialogueView/newDialogue/NewDialogue';
import Archive from './components/dialogueView/archive/Archive';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import './App.css';
import NavBar from './components/navBar/NavBar';
import Home from './components/home/Home';

class App extends Component {
  render() {
    return (
      <Router>
        <div>
          <NavBar />
          <Route exact path="/" component={Home} />
          <Route path="/NewDialogue" component={NewDialogue} />
          <Route path="/Archive" component={Archive} />
          <Route path="/Dialogues" component={DialogueView} />
        </div>
      </Router>
    );
  }
}

export default App;
