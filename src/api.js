import openSocket from 'socket.io-client';
import moment from 'moment';
const  socket = openSocket('http://localhost:8000');
function subscribeToTimer(cb) {
  socket.on('timer', timestamp => cb(null, timestamp));
  socket.emit('subscribeToTimer', 1000);
}

export { subscribeToTimer };

var date = '2018-10-18';
var format = 'LLLL';
var result = moment(date).format(format);
console.log(result);
